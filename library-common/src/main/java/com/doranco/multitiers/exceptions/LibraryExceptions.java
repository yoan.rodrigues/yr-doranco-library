package com.doranco.multitiers.exceptions;

public class LibraryExceptions extends Exception {

	public LibraryExceptions() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LibraryExceptions(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public LibraryExceptions(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LibraryExceptions(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public LibraryExceptions(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
