package com.doranco.multitiers.ejb.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;

import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryExceptions;

@Stateless
public class PanierBean implements Panier {

	List<String> books = new ArrayList<String>(
			Arrays.asList("TwighLight", "20000 Lieu sous la mer", "Harry Potter", "Le voyage dans le temps"));

	@Override
	public void addBook(String book) throws LibraryExceptions {
		// TODO Auto-generated method stub
		books.add(book);
	}

	@Override
	public void removeBook(String book) throws LibraryExceptions {
		// TODO Auto-generated method stub
		try {
			for (String book1 : books) {
				int i = 0;
				if (book == book1) {
					books.remove(i);
				}
				i += 1;
			}

		} catch (Exception e) {

		}
	}

	@Override
	public List<String> getBook() throws LibraryExceptions {
		// TODO Auto-generated method stub
		List<String> results = null;

		try {
			// TODO Va aller recuperer les livres de la BD
			results = books;
		} catch (Exception e) {
			// TODO Loguer la stack strace dans le fichier de logs
			e.printStackTrace();
			throw new LibraryExceptions("Probléme survenu lors de la récupération des livres");

		}
		return results;
	}

}
