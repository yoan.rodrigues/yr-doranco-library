package com.doranco.multitiers.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doranco.multitiers.context.RemoteContext;
import com.doranco.multitiers.ejb.interfaces.Panier;
import com.doranco.multitiers.exceptions.LibraryExceptions;

/**
 * Servlet implementation class Home
 */
@WebServlet("/")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// @EJB(lookup =
	// "corbaname:iiop:localhost:3900#java:global/library-ejb/PanierBean")

	Panier panier;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Home() {
		super();
		try {
			panier = (Panier) RemoteContext.getInstance().getInitialContext()
					.lookup("java:global/library-ear/library-ejb/PanierBean");
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			
			  // Ajout de livre/////////////////////////////////// 
			if ((request.getParameter("addBook")!= "") && (request.getParameter("addBook")!= null)){
				panier.addBook((String)request.getParameter("addBook")); 
			}
			
			  ///////////////////////////////////////////////////
			 
			
			  //Suppression de livre///////////////////////////// 
		
			if ((request.getParameter("removeBook")!= "") && (request.getParameter("removeBook")!= null)){
				panier.removeBook((String) request.getParameter("removeBook")); 
			}
			// removeBook = (String) request.getAttribute("removeBook"); if (!(removeBook ==
			//  "")) { panier.removeBook(removeBook); }
			  
			  ///////////////////////////////////////////////////
			  
			 
			List<String> books = panier.getBook();
			request.setAttribute("books", books);

			this.getServletContext().getRequestDispatcher("/jsp/index.jsp").forward(request, response);
		} catch (LibraryExceptions e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
